import 'dart:ui';

import 'package:bookapp/main.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:intl/intl.dart';
import 'next.dart';
import 'records.dart';

class Details {
  int? member_id;
  String issuerName;
  String bookName;
  String issueDate;
  String dueDate;

  Details({
    this.member_id,
    required this.issuerName,
    required this.bookName,
    required this.dueDate,
    required this.issueDate,
  });

  Map<String, dynamic> toMap() {
    return {
      'member_id': member_id,
      'issuerName': issuerName,
      'bookName': bookName,
      'issueDate': issueDate,
      'dueDate': dueDate,
    };
  }

  @override
  String toString() {
    return '{member_id:$member_id,issuerName:$issuerName,bookName:$bookName,issueDate:$issueDate,dueDate:$dueDate}';
  }
}

class Booking extends StatefulWidget {
  const Booking({super.key});

  @override
  State<Booking> createState() => _BookingState();
}

class _BookingState extends State<Booking> {
  bool edit = false;

  void submit() async {
    if (issuerNameController.text.trim().isNotEmpty &&
        bookNameController.text.trim().isNotEmpty &&
        issueDateController.text.trim().isNotEmpty &&
        dueDateController.text.trim().isNotEmpty) {
      if (!edit) {
        Details obj1 = Details(
          issuerName: issuerNameController.text,
          bookName: bookNameController.text,
          issueDate: issueDateController.text,
          dueDate: dueDateController.text,
        );

        await insertData(obj1);

        setState(() {});
      } //else{

      //   obj!.issuerName = issuerNameController.text.trim();
      //   obj.bookName = bookNameController.text.trim();
      //   obj.issueDate = issueDateController.text.trim();
      //   obj.dueDate = dueDateController.text.trim();

      //   await updateData(obj);
      //   fetchData();
      // }
    }

    clearController();
  }

  void clearController() {
    issuerNameController.clear();
    bookNameController.clear();
    issueDateController.clear();
    dueDateController.clear();
  }

  void removeData(Details obj) {
    setState(() {
      recordList.remove(obj);
    });
  }

  void editData(Details obj) {
    issuerNameController.text = obj.issuerName;
    bookNameController.text = obj.bookName;
    issueDateController.text = obj.issueDate;
    dueDateController.text = obj.dueDate;
  }

  @override
  void dispose() {
    super.dispose();
    issuerNameController.dispose();
    bookNameController.dispose();
    issueDateController.dispose();
    dueDateController.dispose();
  }

  Future<void> fetchData() async {
    // List al = await getLibRecord();
    recordList = await getLibRecord();
    setState(() {});
  }

  TextEditingController issuerNameController = TextEditingController();
  TextEditingController bookNameController = TextEditingController();
  TextEditingController issueDateController = TextEditingController();
  TextEditingController dueDateController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        padding: const EdgeInsets.only(
          left: 20,
          right: 20,
        ),
        decoration: const BoxDecoration(
          gradient: LinearGradient(
            begin: Alignment.topCenter,
            end: Alignment.bottomCenter,
            transform: GradientRotation(20.0),
            colors: [
              Color.fromRGBO(255, 255, 255, 1),
              Color.fromRGBO(239, 247, 248, 0.98),
              Color.fromRGBO(236, 245, 247, 0.98),
              Color.fromRGBO(225, 245, 250, 0.62),
            ],
          ),
        ),
        child: SingleChildScrollView(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              const SizedBox(
                height: 60,
              ),
              Center(
                child: Text(
                  "Book Issuing Details",
                  style: GoogleFonts.quicksand(
                    fontSize: 22,
                    fontWeight: FontWeight.w700,
                  ),
                ),
              ),
              const SizedBox(
                height: 40,
              ),
              Text(
                "Issuer's Name",
                style: GoogleFonts.quicksand(
                  fontWeight: FontWeight.bold,
                  color: const Color.fromRGBO(0, 139, 148, 1),
                ),
              ),
              const SizedBox(
                height: 10,
              ),
              TextFormField(
                controller: issuerNameController,
                decoration: InputDecoration(
                  filled: true,
                  fillColor: Colors.white,
                  hintText: "Enter Issuer's Name",
                  focusedBorder: OutlineInputBorder(
                    borderRadius: BorderRadius.circular(12),
                    borderSide: const BorderSide(
                      color: Color.fromRGBO(0, 139, 148, 1),
                    ),
                  ),
                  border: OutlineInputBorder(
                    borderRadius: BorderRadius.circular(12),
                  ),
                ),
              ),
              const SizedBox(
                height: 10,
              ),
              Text(
                "Book Name",
                style: GoogleFonts.quicksand(
                  fontWeight: FontWeight.bold,
                  color: const Color.fromRGBO(0, 139, 148, 1),
                ),
              ),
              const SizedBox(
                height: 10,
              ),
              TextFormField(
                controller: bookNameController,
                decoration: InputDecoration(
                  filled: true,
                  fillColor: Colors.white,
                  hintText: "Enter Book Name",
                  focusedBorder: OutlineInputBorder(
                    borderRadius: BorderRadius.circular(12),
                    borderSide: const BorderSide(
                      color: Color.fromRGBO(0, 139, 148, 1),
                    ),
                  ),
                  border: OutlineInputBorder(
                    borderRadius: BorderRadius.circular(12),
                  ),
                ),
              ),
              const SizedBox(
                height: 10,
              ),
              Text(
                "Issue Date",
                style: GoogleFonts.quicksand(
                  fontWeight: FontWeight.bold,
                  color: const Color.fromRGBO(0, 139, 148, 1),
                ),
              ),
              const SizedBox(
                height: 10,
              ),
              TextField(
                  controller: issueDateController,
                  readOnly: true,
                  decoration: InputDecoration(
                    filled: true,
                    fillColor: Colors.white,
                    hintText: "Enter issue Date",
                    suffixIcon: const Icon(Icons.date_range_rounded),
                    focusedBorder: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(12),
                      borderSide: const BorderSide(
                        color: Color.fromRGBO(0, 139, 148, 1),
                      ),
                    ),
                    border: OutlineInputBorder(
                      borderSide: const BorderSide(
                        color: Colors.purpleAccent,
                      ),
                      borderRadius: BorderRadius.circular(12),
                    ),
                  ),
                  onTap: () async {
                    DateTime? pickedDate = await showDatePicker(
                      context: context,
                      initialDate: DateTime.now(),
                      firstDate: DateTime(2024),
                      lastDate: DateTime(2025),
                    );

                    String formatedDate =
                        DateFormat.yMMMd().format(pickedDate!);

                    setState(() {
                      issueDateController.text = formatedDate;
                    });
                  }),
              const SizedBox(
                height: 10,
              ),
              Text(
                "Due Date",
                style: GoogleFonts.quicksand(
                  fontWeight: FontWeight.bold,
                  color: const Color.fromRGBO(0, 139, 148, 1),
                ),
              ),
              const SizedBox(
                height: 10,
              ),
              TextField(
                  controller: dueDateController,
                  readOnly: true,
                  decoration: InputDecoration(
                    filled: true,
                    fillColor: Colors.white,
                    hintText: "Enter Due Date",
                    suffixIcon: const Icon(Icons.date_range_rounded),
                    focusedBorder: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(12),
                      borderSide: const BorderSide(
                        color: Color.fromRGBO(0, 139, 148, 1),
                      ),
                    ),
                    border: OutlineInputBorder(
                      borderSide: const BorderSide(
                        color: Colors.purpleAccent,
                      ),
                      borderRadius: BorderRadius.circular(12),
                    ),
                  ),
                  onTap: () async {
                    DateTime? pickedDate = await showDatePicker(
                      context: context,
                      initialDate: DateTime.now(),
                      firstDate: DateTime(2024),
                      lastDate: DateTime(2025),
                    );

                    String formatedDate =
                        DateFormat.yMMMd().format(pickedDate!);

                    setState(() {
                      dueDateController.text = formatedDate;
                    });
                  }),
              const SizedBox(
                height: 40,
              ),
              SizedBox(
                height: 65,
                width: 350,
                child: ElevatedButton(
                  style: ElevatedButton.styleFrom(
                    backgroundColor: const Color.fromRGBO(232, 144, 12, 1),
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(20),
                    ),
                  ),
                  onPressed: () async {
                    // Details obj = Details(
                    //   issuerName: issuerNameController.text.trim(),
                    //   bookName: bookNameController.text.trim(),
                    //   issueDate: issueDateController.text.trim(),
                    //   dueDate: dueDateController.text.trim(),
                    // );

                    submit();
                    await fetchData();
                    setState(() {});
                    Navigator.push(context,
                        MaterialPageRoute(builder: (context) => Records()));
                  },
                  child: const Text(
                    "Submit",
                    style: TextStyle(
                      color: Colors.white,
                      fontSize: 22,
                    ),
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
