//import 'package:bookapp/dummy.dart';
import 'package:bookapp/home.dart';
import 'package:bookapp/next.dart';
import 'package:bookapp/records.dart';
import 'package:flutter/material.dart';
import 'bookissue.dart';
import 'records.dart';

class Navigation extends StatefulWidget {
  const Navigation({super.key});

  @override
  State<Navigation> createState() => _NavigationState();
}

class _NavigationState extends State<Navigation> {
  List<Widget> widgetList = [
    NextPage(),
    Booking(),
    Records(),
  ];

  int selectedIndex = 0;

  void navigateBottomBar(int index) {
    setState(() {
      selectedIndex = index;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: widgetList[selectedIndex],
      bottomNavigationBar: BottomNavigationBar(
        currentIndex: selectedIndex,
        onTap: navigateBottomBar,
        items: const [
          BottomNavigationBarItem(
            icon: Icon(Icons.home),
            label: "Home",
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.add),
            label: "Add",
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.receipt),
            label: "Records",
          ),
        ],
      ),
    );
  }
}
