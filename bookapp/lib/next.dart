import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'home.dart';
import 'records.dart';
import 'bookissue.dart';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
// import 'package:book_app/bookissue.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter/widgets.dart';
import 'package:google_fonts/google_fonts.dart';

class NextPage extends StatefulWidget {
  const NextPage({super.key});

  @override
  State<NextPage> createState() => _NextPageState();
}

class _NextPageState extends State<NextPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        padding: const EdgeInsets.only(
          left: 20,
          right: 20,
        ),
        decoration: const BoxDecoration(
          gradient: LinearGradient(
            begin: Alignment.topCenter,
            end: Alignment.bottomCenter,
            transform: GradientRotation(20.0),
            colors: [
              Color.fromRGBO(255, 255, 255, 1),
              Color.fromRGBO(239, 247, 248, 0.98),
              Color.fromRGBO(236, 245, 247, 0.98),
              Color.fromRGBO(225, 245, 250, 0.62),
            ],
          ),
        ),
        child: SingleChildScrollView(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              const SizedBox(
                height: 36,
              ),
              const Text(
                "Hi ",
                style: TextStyle(
                  fontFamily: 'Mochiy Pop One',
                  fontSize: 36,
                  fontWeight: FontWeight.w700,
                ),
              ),
              const Text(
                "Pranav ",
                style: TextStyle(
                  fontFamily: 'Mochiy Pop One',
                  fontSize: 36,
                  fontWeight: FontWeight.w700,
                ),
              ),
              const SizedBox(
                height: 10,
              ),
              SingleChildScrollView(
                scrollDirection: Axis.horizontal,
                child: Row(
                  children: [
                    Image.asset(
                      width: 350,
                      height: 230,
                      "assets/images/images/thought1.jpg",
                    ),
                    const SizedBox(
                      width: 20,
                    ),
                    Image.asset(
                      width: 350,
                      height: 230,
                      "assets/images/images/Thought2.jpg",
                    ),
                    const SizedBox(
                      width: 20,
                    ),
                    Image.asset(
                      width: 350,
                      height: 230,
                      "assets/images/images/thought3.jpg",
                    ),
                  ],
                ),
              ),
              const SizedBox(
                height: 20,
              ),
              Container(
                padding: const EdgeInsets.all(10),
                width: 130,
                height: 40,
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.all(Radius.circular(30)),
                  color: Colors.white,
                  border: Border.all(
                    color: Colors.black,
                    width: 2,
                  ),
                ),
                child: const Text(
                  "Computer Science",
                  style: TextStyle(
                    color: Colors.black,
                    fontWeight: FontWeight.w700,
                    fontSize: 12,
                  ),
                ),
              ),
              const SizedBox(
                height: 20,
              ),
              SingleChildScrollView(
                scrollDirection: Axis.horizontal,
                child: Row(
                  children: [
                    Image.asset(
                      width: 150,
                      height: 200,
                      "assets/images/images/comp1.jpg",
                    ),
                    const SizedBox(
                      width: 10,
                    ),
                    Image.asset(
                      width: 150,
                      height: 200,
                      "assets/images/images/comp2.jpg",
                    ),
                    const SizedBox(
                      width: 10,
                    ),
                    Image.asset(
                      width: 150,
                      height: 200,
                      "assets/images/images/comp3.jpg",
                    ),
                    const SizedBox(
                      width: 10,
                    ),
                    Image.asset(
                      width: 150,
                      height: 200,
                      "assets/images/images/comp4.jpg",
                    ),
                    const SizedBox(
                      width: 10,
                    ),
                    Image.asset(
                      width: 150,
                      height: 200,
                      "assets/images/images/comp5.jpg",
                    ),
                  ],
                ),
              ),
              const SizedBox(
                height: 20,
              ),
              Container(
                padding: const EdgeInsets.all(10),
                width: 130,
                height: 40,
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.all(Radius.circular(30)),
                  color: Colors.white,
                  border: Border.all(
                    color: Colors.black,
                    width: 2,
                  ),
                ),
                child: const Text(
                  "AIML",
                  style: TextStyle(
                    color: Colors.black,
                    fontWeight: FontWeight.w700,
                    fontSize: 12,
                  ),
                ),
              ),
              const SizedBox(
                height: 20,
              ),
              SingleChildScrollView(
                scrollDirection: Axis.horizontal,
                child: Row(
                  children: [
                    Image.asset(
                      width: 150,
                      height: 200,
                      "assets/images/images/aiml1.jpg",
                    ),
                    const SizedBox(
                      width: 10,
                    ),
                    Image.asset(
                      width: 150,
                      height: 200,
                      "assets/images/images/aiml2.jpg",
                    ),
                    const SizedBox(
                      width: 10,
                    ),
                    Image.asset(
                      width: 150,
                      height: 200,
                      "assets/images/images/aiml3.jpg",
                    ),
                    const SizedBox(
                      width: 10,
                    ),
                    Image.asset(
                      width: 150,
                      height: 200,
                      "assets/images/images/aiml4.jpg",
                    ),
                    const SizedBox(
                      width: 10,
                    ),
                    Image.asset(
                      width: 150,
                      height: 200,
                      "assets/images/images/aiml5.jpg",
                    ),
                  ],
                ),
              ),
              const SizedBox(
                height: 30,
              ),
            ],
          ),
        ),
      ),
    );
  }
}
