import 'package:bookapp/navigation.dart';
import 'package:flutter/material.dart';
import 'next.dart';

class Login extends StatefulWidget {
  const Login({Key? key}) : super(key: key);

  @override
  State<Login> createState() => _LoginState();
}

class _LoginState extends State<Login> {
  List<Map<String, String>> authentication = [
    {"username": "mohitbawankar2001@gmail.com", "password": "Mohit@123"},
    {"username": "abhaypatil31@gmail.com", "password": "Abhay@123"},
    {"username": "pranavbochare123@gmail.com", "password": "Pranav@123"},
  ];
  TextEditingController usernameController = TextEditingController();
  TextEditingController passwordController = TextEditingController();

  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();

  bool isAuthenticated = false;

  void handleLogin() {
    bool loginValidated = _formKey.currentState!.validate();

    for (var i in authentication) {
      if (loginValidated &&
          i['username'] == usernameController.text &&
          i['password'] == passwordController.text) {
        isAuthenticated = true;
        break;
      }
    }

    if (isAuthenticated) {
      ScaffoldMessenger.of(context).showSnackBar(
        const SnackBar(
          content: Text("Login Successful !!!"),
        ),
      );
    } else {
      ScaffoldMessenger.of(context).showSnackBar(
        const SnackBar(
          content: Text("Login failed !!!"),
        ),
      );
    }
    setState(() {});
  }

  @override
  Widget build(BuildContext context) {
    if (isAuthenticated) {
      return Navigation();
    } else {
      return Scaffold(
        body: SingleChildScrollView(
          child: Container(
            decoration: const BoxDecoration(
              gradient: LinearGradient(
                begin: Alignment.topCenter,
                end: Alignment.bottomCenter,
                transform: GradientRotation(20.0),
                colors: [
                  Color.fromRGBO(255, 255, 255, 1),
                  Color.fromRGBO(239, 247, 248, 0.98),
                  Color.fromRGBO(236, 245, 247, 0.98),
                  Color.fromRGBO(225, 245, 250, 0.62),
                ],
              ),
            ),
            child: Column(
              children: [
                const SizedBox(
                  height: 50,
                ),
                Center(
                  child: Image.asset(
                    "assets/images/images/img2.png",
                  ),
                ),
                Container(
                  width: 200,
                  height: 100,
                  child: const Column(
                    children: [
                      Text(
                        "Book",
                        style: TextStyle(
                            color: Color.fromRGBO(161, 52, 144, 1),
                            fontSize: 34,
                            fontFamily: 'milonga',
                            fontWeight: FontWeight.bold),
                      ),
                      Text(
                        "Mate",
                        style: TextStyle(
                            color: Color.fromRGBO(164, 39, 144, 1),
                            fontSize: 34,
                            fontFamily: 'milonga',
                            fontWeight: FontWeight.bold),
                      ),
                    ],
                  ),
                ),
                const SizedBox(
                  height: 50,
                ),
                Padding(
                  padding: const EdgeInsets.only(
                    left: 40,
                    right: 40,
                  ),
                  child: Form(
                    key: _formKey,
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        TextFormField(
                          controller: usernameController,
                          decoration: InputDecoration(
                              filled: true,
                              fillColor: Colors.white,
                              hintText: "Enter username",
                              label: const Text("Username"),
                              border: OutlineInputBorder(
                                borderRadius: BorderRadius.circular(40),
                              ),
                              prefixIcon: const Icon(
                                Icons.person,
                              )),
                          validator: (value) {
                            if (value == null || value.isEmpty) {
                              return "Please Enter username!!!";
                            } else {
                              return null;
                            }
                          },
                        ),
                        const SizedBox(
                          height: 15,
                        ),
                        TextFormField(
                          controller: passwordController,
                          obscureText: true,
                          obscuringCharacter: "*",
                          decoration: InputDecoration(
                            filled: true,
                            fillColor: Colors.white,
                            hintText: "Enter Password",
                            label: const Text("Password"),
                            border: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(40),
                            ),
                            prefixIcon: const Icon(
                              Icons.lock,
                            ),
                            suffixIcon: const Icon(
                              Icons.remove_red_eye_outlined,
                            ),
                          ),
                          validator: (value) {
                            if (value == null || value.isEmpty) {
                              return "Please Enter password!!";
                            } else {
                              return null;
                            }
                          },
                        ),
                        const SizedBox(
                          height: 60,
                        ),
                        SizedBox(
                          width: 320,
                          height: 50,
                          child: ElevatedButton(
                            style: ElevatedButton.styleFrom(
                              shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(8),
                              ),
                              backgroundColor:
                                  const Color.fromRGBO(232, 144, 12, 1),
                            ),
                            onPressed: () {
                              handleLogin();
                            },
                            child: const Text(
                              "Login",
                              style: TextStyle(
                                fontSize: 24,
                                fontWeight: FontWeight.w700,
                                color: Colors.white,
                              ),
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
              ],
            ),
          ),
        ),
      );
    }
  }
}
