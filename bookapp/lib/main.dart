import 'package:flutter/material.dart';
import 'home.dart';
import 'package:sqflite/sqflite.dart';
import 'package:path/path.dart';
import 'bookissue.dart';

dynamic database;
List<Details> dbList = [];

void main() async {
  WidgetsFlutterBinding.ensureInitialized();

  database = await openDatabase(
    join(await getDatabasesPath(), 'libraryDB1.db'),
    onCreate: (db, version) {
      return db.execute('''
          CREATE TABLE libRec(member_id INTEGER PRIMARY KEY AUTOINCREMENT, issuerName TEXT,bookName TEXT, issueDate Date,dueDate Date)
        ''');
    },
    version: 1,
  );

  dbList = await getLibRecord();
  print(dbList);
  runApp(const MainApp());
}

Future<void> insertData(Details obj) async {
  final db = await database;

  await db.insert(
    'libRec',
    obj.toMap(),
    conflictAlgorithm: ConflictAlgorithm.replace,
  );

  print("abcd:${await getLibRecord()}");
}

Future<List<Details>> getLibRecord() async {
  final db = await database;

  final List<Map<String, dynamic>> libMaps = await db.query('libRec');

  return List.generate(libMaps.length, (i) {
    return Details(
      member_id: libMaps[i]['member_id'],
      issuerName: libMaps[i]['issuerName'],
      bookName: libMaps[i]['bookName'],
      issueDate: libMaps[i]['issueDate'],
      dueDate: libMaps[i]['dueDate'],
    );
  });
}

Future<void> updateData(Details obj) async {
  final db = await database;

  await db.update(
    'libRec',
    obj.toMap(),
    where: 'member_id  =?',
    whereArgs: [obj.member_id],
  );
}



class MainApp extends StatelessWidget {
  const MainApp({super.key});

  @override
  Widget build(BuildContext context) {
    return const MaterialApp(
      debugShowCheckedModeBanner: false,
      home: Login(),
    );
  }
}
