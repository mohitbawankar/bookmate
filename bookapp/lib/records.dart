import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:google_fonts/google_fonts.dart';
import 'bookissue.dart';
import 'main.dart';

class Records extends StatefulWidget {
  const Records({super.key});

  @override
  State<StatefulWidget> createState() => _RecordsState();
}

List<Details> recordList = dbList;

class _RecordsState extends State<Records> {
  // bool isTapped = false;

  List<bool> checked = [
    false,
    false,
    false,
    false,
    false,
    false,
    false,
    false,
    false,
    false,
    false,
    false,
    false,
    false,
    false,
    false,
    false,
    false,
    false,
    false,
    false,
    false,
    false,
    false,
    false,
    false,
    false,
    false,
    false,
    false,
    false,
    false,
    false,
    false,
    false,
    false,
    false,
    false,
    false,
    false,
    false,
    false,
    false,
    false,
    false,
    false,
    false,
    false,
    false,
    false,
    false,
    false,
    false,
    false,
    false,
    false,
    false,
    false,
    false,
    false,
    false,
    false,
    false,
  ];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        constraints: const BoxConstraints.expand(),
        decoration: const BoxDecoration(
          gradient: LinearGradient(
            begin: Alignment.topCenter,
            end: Alignment.bottomCenter,
            transform: GradientRotation(20.0),
            colors: [
              Color.fromRGBO(255, 255, 255, 1),
              Color.fromRGBO(239, 247, 248, 0.98),
              Color.fromRGBO(236, 245, 247, 0.98),
              Color.fromRGBO(225, 245, 250, 0.62),
            ],
          ),
        ),
        child: Column(
          children: [
            const SizedBox(
              height: 50,
            ),
            Center(
              child: Text(
                "Records",
                style: GoogleFonts.quicksand(
                  fontSize: 22,
                  fontWeight: FontWeight.bold,
                ),
              ),
            ),
            const SizedBox(
              height: 20,
            ),
            SizedBox(
              height: 590,
              child: ListView.builder(
                itemCount: recordList.length,
                itemBuilder: (context, index) {
                  return Padding(
                    padding: const EdgeInsets.only(
                      top: 15,
                      left: 10,
                      right: 10,
                    ),
                    child: Container(
                      height: 110,
                      decoration: const BoxDecoration(
                          boxShadow: [
                            BoxShadow(
                              color: Colors.grey,
                              blurRadius: 2,
                              offset: Offset(2, 2),
                              spreadRadius: 2,
                            ),
                          ],
                          color: Color(0xffFAE8E8),
                          borderRadius: BorderRadius.all(Radius.circular(10))),
                      child: Padding(
                        padding:
                            const EdgeInsets.only(left: 12, top: 5, bottom: 3),
                        child: Row(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Column(
                              children: [
                                Container(
                                  width: 270,
                                  height: 20,
                                  child: Text(
                                    // "123",
                                    "Issuer's Name   : ${recordList[index].issuerName}",
                                    style: GoogleFonts.quicksand(
                                      fontSize: 16,
                                      fontWeight: FontWeight.bold,
                                    ),
                                  ),
                                ),
                                Container(
                                  width: 270,
                                  height: 20,
                                  child: Text(
                                    // "123",
                                    "Book Name       : ${recordList[index].bookName}",
                                    style: GoogleFonts.quicksand(
                                      fontSize: 16,
                                      fontWeight: FontWeight.bold,
                                    ),
                                  ),
                                ),
                                Container(
                                  width: 270,
                                  height: 20,
                                  child: Text(
                                    // "232",
                                    "Issue Date         : ${recordList[index].issueDate}",
                                    style: GoogleFonts.quicksand(
                                      fontSize: 16,
                                      fontWeight: FontWeight.bold,
                                    ),
                                  ),
                                ),
                                Container(
                                  width: 270,
                                  height: 20,
                                  child: Text(
                                    // "2434",
                                    "Due Date           : ${recordList[index].dueDate}",
                                    style: GoogleFonts.quicksand(
                                      fontSize: 16,
                                      fontWeight: FontWeight.bold,
                                    ),
                                  ),
                                ),
                              ],
                            ),
                            Checkbox(
                              shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(10),
                              ),
                              // value: isTapped,
                              value: checked[index],
                              onChanged: (value) {
                                setState(() {
                                  // isTapped = value!;
                                  checked[index] = true;
                                });
                              },
                              activeColor: Colors.green,
                            ),
                          ],
                        ),
                      ),
                    ),
                  );
                },
              ),
            )
          ],
        ),
      ),
    );
  }
}
